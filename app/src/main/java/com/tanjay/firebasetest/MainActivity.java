package com.tanjay.firebasetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView name;
    TextView nama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nama = (TextView) findViewById(R.id.nama);
        name = (TextView) findViewById(R.id.name);

        if(getIntent().getExtras() != null){
            for (String key : getIntent().getExtras().keySet()){
                if(key.equals("name")){
                    nama.setText(getIntent().getExtras().getString(key));
                }else if(key.equals("gh")){
                    name.setText(getIntent().getExtras().getString(key));

                }
            }
        }
    }
}
